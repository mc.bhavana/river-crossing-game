import pygame
import sys

from config import *

#initialise the pygame
pygame.init()
pygame.font.init()
#time counter
import time
import math
#create the screen
screenwidth=800
screenheight=600
screen = pygame.display.set_mode((screenwidth,screenheight))
clock=pygame.time.Clock()
# title
pygame.display.set_caption("Scary River")
x=370
y=560

#welcome screen

def welcome():
    screen.fill(black)
    winnerP1Message = font.render(m1, True, white)
    screen.blit(winnerP1Message, ((400 - (winnerP1Message.get_width()/2)),(300 - (winnerP1Message.get_height()/2))))
    pygame.display.update()
    pygame.time.delay(500)

#cordinates for all the obstacles 
x2=280
y2=0
x3=400
y3=350
x4=100
y4=470
x5=700
y5=230
x6=50
y6=110
x7=500
y7=110
width=40
height=40
val=10
jump=False
jumpcount=10


#player position for swap
px=370
py=560

#score board
font = pygame.font.Font(f, 25)
#score list
points=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
#stri="Score: "+str(score)

#intilaisation for moving obstacles
ship=pygame.image.load('ship.png') 
ship=pygame.transform.scale(ship,(50,50))
ob1=0
ob2=100
ob3=450
ob4=110
ob5=600
speed=90
endx=600
endy=20

#player
playerImg1=pygame.image.load('super.png') 
playerImg1=pygame.transform.scale(playerImg1,(40,40))
devil1=pygame.image.load('devil2.png')
devil1=pygame.transform.scale(devil1,(40,40))
devil2=pygame.image.load('cerberus.png')
devil2=pygame.transform.scale(devil2,(40,45))
devil3=pygame.image.load('devil1.png')
devil3=pygame.transform.scale(devil3,(40,40))

#text

font = pygame.font.Font(f, 25)


play=0

#making score reset for player2
def zero():
    for i in range(15):
        points[i]=0  


def winnerPlayer():
    screen.fill(black)
    winnerP1Message = font.render(m2, True, white)
    screen.blit(winnerP1Message, ((400 - (winnerP1Message.get_width()/2)),(300 - (winnerP1Message.get_height()/2))))
    pygame.display.update()
    pygame.time.delay(1000)
    #startscreen()

def fail():
    screen.fill(black)
    winnerP1Message = font.render(m3, True, white)
    screen.blit(winnerP1Message, ((400 - (winnerP1Message.get_width()/2)),(300 - (winnerP1Message.get_height()/2))))
    pygame.display.update()
    pygame.time.delay(1000)
    #startscreen()

#time
minutes = 0
seconds = 0
milliseconds = 0
t0=time.time()
#score1=score2=0
#time1=time2=0

welcome()
count=0
# Game loop
run = True
while run:
    #milliseconds=clock.tick()
    #seconds=milliseconds//1000
    #minutes=seconds//60
    #print ("{}:{}".format(minutes, seconds))
    #text4=str(seconds)
    t1=time.time()
    d=(t1-t0)
    dt=math.floor(d)
    tim=str(dt)
    time1=tim
    time2=tim
    #timsec=str(seconds)
    #timmin=str(minutes)
    #tim=timmin+':'+timsec
    text4=font.render(tim,True,black)
    textRect4=text4.get_rect()
    textRect4.center = (700,590)

    pygame.time.delay(70)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            # to close the game
            run = False

    #text
    text = font.render(s, True, black,g)
    text2 = font.render(e,True,black,r)
    textRect = text.get_rect()
    textRect2=text2.get_rect()
    textRect.center = (370-40,560+25)
    textRect2.center=(endx-100,endy)
    #score display
    textRect3=text2.get_rect()
    textRect3.center = (endx-500,endy)

    #operating with keys
    keys = pygame.key.get_pressed()

    if keys[pygame.K_LEFT] and x>val:
        x=x-val
    if keys[pygame.K_RIGHT] and x<screenwidth-width-val:
        x=x+val
    if not(jump):
        if keys[pygame.K_UP] and y > val:
            y=y-val
        if keys[pygame.K_DOWN] and y < screenheight-height-val:
            y=y+val
        if keys[pygame.K_SPACE]:
            jump=True 
    else:
        if jumpcount >= -10:
            neg=1
            if jumpcount < 0:
                neg=-1
            y -= (jumpcount**2) * 0.5 * neg
            jumpcount-=1
        else:
            jump=False
            jumpcount=10

    #moving obstacles
    screen.fill(blue)
    screen.blit(ship,(ob1,50))
    screen.blit(ship,(ob2,160))
    screen.blit(ship,(ob3,280))
    screen.blit(ship,(ob4,400))
    screen.blit(ship,(ob5,510))
    time_passed=clock.tick()
    time_seconds=time_passed/1000.0
    dist=time_seconds*speed
    ob1+=dist
    ob2+=dist
    ob3+=dist
    ob4+=dist
    ob5+=dist
    if(ob1>screenwidth):
        ob1=0
    if(ob2>screenwidth):
        ob2=0
    if(ob3>screenwidth):
        ob3=0
    if(ob4>screenwidth):
        ob4=0
    if(ob5>screenwidth):
        ob5=0


    #The following if and else is for if any obstacle reaches player then it goes away from it
    #position of player1 in bottom
    #x = startx 
    #y = 560
    #if player 1 collide with any obastacle then player2 will start at top
    #also swapping player position
    #pp=560
    #counter to check the no of iterations
    #count=0
    startx=600
    if x+35>ob1 and x-35<ob1:
        if y+35>50 and y-35<50:
            if px==370 and py==560:
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            tim=0
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            t0=t1
            zero()
            count=count+1
            fail()

    elif x+35>ob2 and x-35<ob2:
        if y+35>160 and y-35<160: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            t0=t1
            zero()
            count=count+1
            fail()
    elif x+35>ob3 and x-35<ob3:
        if y+35>280 and y-35<280: 
           if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
           else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
           temp=s
           s=e
           e=temp
           temp_color=r
           r=g
           g=temp_color
           zero()
           t0=t1
           count=count+1
           fail()
    elif x+35>ob4 and x-35<ob4:
        if y+35>400 and y-35<400: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    elif x+35>ob5 and x-35<ob5:
        if y+35>510 and y-35<510: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    #for fixed obstacles
    elif x+35>280 and x-35<280:
        if y+35>0 and y-35<0: 
           if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
           else:
               x=370
               y=560
               px=x
               py=y
               play=0
               time2=tim
           temp=s
           s=e
           e=temp
           temp_color=r
           r=g
           g=temp_color
           zero()
           t0=t1
           count=count+1
           fail()
    elif x+35>50 and x-20<50:
        if y+35>110 and y-35<110: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    elif x+35>500 and x-35<500:
        if y+35>110 and y-35<110: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    elif x+35>700 and x-35<700:
        if y+35>230 and y-35<230: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    elif x+35>10 and x-35<10:
        if y+35>230 and y-35<230: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
               x=370
               y=560
               px=x
               py=y
               play=0
               time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    elif x+35>100 and x-35<100:
        if y+35>470 and y-35<470: 
           if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
           else:
               x=370
               y=560
               px=x
               py=y
               play=0
               time2=tim
           temp=s
           s=e
           e=temp
           temp_color=r
           r=g
           g=temp_color
           zero()
           t0=t1
           count=count+1
           fail()
    elif x+35>700 and x-35<700:
        if y+35>-10 and y-35<-10: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    elif x+35>10 and x-35<10:
        if y+35>560 and y-35<560: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()
    elif x+35>400 and x-35<400:
        if y+35>350 and y-35<350: 
           if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
           else:
               x=370
               y=560
               px=x
               py=y
               play=0
               time2=tim
           temp=s
           s=e
           e=temp
           temp_color=r
           r=g
           g=temp_color
           zero()
           t0=t1
           count=count+1
           fail()
    elif x+35>660 and x-35<660:
        if y+35>470 and y-35<470: 
            if(px==370 and py==560):
                x = startx
                y = 0
                px=x
                py=y
                play=1
                time1=tim
            else:
                x=370
                y=560
                px=x
                py=y
                play=0
                time2=tim
            temp=s
            s=e
            e=temp
            temp_color=r
            r=g
            g=temp_color
            zero()
            t0=t1
            count=count+1
            fail()

     # calculating score for palyer 1 for fixed obstacles
    if(play==0):
        if(y<530):
            points[0]=True
        if(y<440):
            points[1]=True
            points[2]=True
        if(y<330):
            points[3]=True
        if(y<190):
            points[4]=True
            points[5]=True
        if(y<80):
            points[6]=True
            points[7]=True
        if(y<25):
            points[8]=True
            points[9]=True
        #for moving obstacles
        if(y<505):
            points[10]=True
        if(y<400):
            points[11]=True
        if(y<290):
            points[12]=True
        if(y<170):
            points[13]=True
        if(y<35):
            points[14]=True

    sscore=0
    score=0
    for i in range(10):
        if(points[i]==True):
            sscore+=5
            #print(sscore)
    stri='Score: '+str(sscore)  
    i=10
    mscore=0
    for i in range(10,15):
        if(points[i]==True):
            mscore+=10
            #print(mscore)
    fscore=mscore
    score=sscore+mscore
    stri='Score: '+str(score)


    #calculating scores for player2 top player
    if(play==1):
        if(y>30):
            points[0]=True
            points[1]=True
        if(y>135):
            points[2]=True
            points[3]=True
        if(y>270):
            points[4]=True
            points[5]=True
        if(y>370):
            points[6]=True
        if(y>490):
            points[7]=True
            points[8]=True
        if(y>530):
            points[9]=True
        #for moving obastacles
        if(y>100):
            points[10]=True
        if(y>230):
            points[11]=True
        if(y>350):
            points[12]=True
        if(y>470):
            points[13]=True
        if(y>540):
            points[14]=True

    sscore=0
    score1=0
    for i in range(10):
        if(points[i]==True):
            sscore+=5
            #print(sscore)
    stri='Score: '+str(sscore)  
    i=10
    mscore=0
    for i in range(10,15):
        if(points[i]==True):
            mscore+=10
            #print(mscore)
    fscore=mscore
    score1=sscore+mscore
    stri='Score: '+str(score1)
    #print(st)

    #if player score is maximum then the player is win.
    if(score==100) or (score1==100):
        if(px==370 and py==560):
            x = startx
            y = 0
            px=x
            py=y
            play=1
            time1=tim
            #score1=score
            #time1=tim
        else:
            x=370
            y=560
            px=x
            py=y
            play=0
            time2=tim
            #score2=score
            #time2=tim
        temp=s
        s=e
        e=temp
        temp_color=r
        r=g
        g=temp_color
        zero()
        t0=t1
        count=count+1
        #winnerPlayerOne()

    #print(score)
    #print(score1)
    if(count==2):
        if(score<score1) and (time1>time2):
            print("player2 is winner")
            vel=vel+10
        else:
            print("player1 is winner")
           


    #text3=stri
    text3 = font.render(stri,True,black)
  

       #drawing the fixed rectangles
    pygame.draw.rect(screen,orange, (0,0,screenwidth,35))
    pygame.draw.rect(screen,orange, (0,120,screenwidth,35))  
    pygame.draw.rect(screen,orange, (0,240,screenwidth,35))  
    pygame.draw.rect(screen,orange, (0,360,screenwidth,35))  
    pygame.draw.rect(screen,orange, (0,480,screenwidth,35))
    pygame.draw.rect(screen,orange, (0,565,screenwidth,35))
     #text blit
    screen.blit(text, textRect)
    screen.blit(text2, textRect2) 
    #screen.blit(text3, textRect3) 

    #display score
    screen.blit(text3, textRect3) 
    #display time counter
    screen.blit(text4,textRect4)
  
    screen.blit(playerImg1,(x,y))
    pygame.display.flip()
    
    screen.blit(devil1,(x2,y2))
    screen.blit(devil2,(x3,y3))
    screen.blit(devil3,(x4,y4))
    screen.blit(devil3,(x5,y5))
    screen.blit(devil2,(x6,y6))
    screen.blit(devil1,(x7,y7))
    screen.blit(devil2,(700,-10))
    screen.blit(devil1,(10,230))
    screen.blit(devil2,(10,560))
    screen.blit(devil2,(660,470))
    pygame.display.update()

pygame.quit()
